"""ScoDoc 9 models : Formation BUT 2021
    XXX inutilisé
"""

from app import db


class APCFormation(db.Model):
    """Formation par compétence"""

    id = db.Column(db.Integer, primary_key=True)
    dept_id = db.Column(db.Integer, db.ForeignKey("departement.id"), index=True)
    specialite = db.Column(db.Text(), nullable=False)  # "RT"
    specialite_long = db.Column(
        db.Text(), nullable=False
    )  # "Réseaux et télécommunications"
