// Mécanisme d'auto-complétion (choix) d'un étudiant
// Il faut un champs #etudiant (text input) et à coté un champ hidden etudid qui sera rempli.
// utilise autoComplete.js, source https://tarekraafat.github.io/autoComplete.js
// EV 2023-06-01

function etud_autocomplete_config(with_dept = false) {
  return {
    selector: "#etudiant",
    placeHolder: "Nom...",
    threshold: 3,
    data: {
      src: async (query) => {
        try {
          // Fetch Data from external Source
          const source = await fetch(`/ScoDoc/api/etudiants/name/${query}`);
          // Data should be an array of `Objects` or `Strings`
          const data = await source.json();
          return data;
        } catch (error) {
          return error;
        }
      },
      // Data source 'Object' key to be searched
      keys: ["nom"],
    },
    events: {
      input: {
        selection: (event) => {
          const prenom = sco_capitalize(event.detail.selection.value.prenom);
          const selection = with_dept
            ? `${event.detail.selection.value.nom} ${prenom} (${event.detail.selection.value.dept_acronym})`
            : `${event.detail.selection.value.nom} ${prenom}`;
          // store etudid
          const etudidField = document.getElementById("etudid");
          etudidField.value = event.detail.selection.value.id;
          autoCompleteJS.input.value = selection;
        },
      },
    },
    resultsList: {
      element: (list, data) => {
        if (!data.results.length) {
          // Create "No Results" message element
          const message = document.createElement("div");
          // Add class to the created element
          message.setAttribute("class", "no_result");
          // Add message text content
          message.innerHTML = `<span>Pas de résultat pour "${data.query}"</span>`;
          // Append message element to the results list
          list.prepend(message);
          // Efface l'etudid
          const etudidField = document.getElementById("etudid");
          etudidField.value = "";
        }
      },
      noResults: true,
    },
    resultItem: {
      highlight: true,
      element: (item, data) => {
        const prenom = sco_capitalize(data.value.prenom);
        item.innerHTML += with_dept
          ? ` ${prenom} (${data.value.dept_acronym})`
          : ` ${prenom}`;
      },
    },
  };
}
