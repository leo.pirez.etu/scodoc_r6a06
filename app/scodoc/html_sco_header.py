# -*- mode: python -*-
# -*- coding: utf-8 -*-

##############################################################################
#
# Gestion scolarite IUT
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################

"""HTML Header/Footer for ScoDoc pages
"""

import html

from flask import g, render_template
from flask import request
from flask_login import current_user

import app.scodoc.sco_utils as scu
from app import scodoc_flash_status_messages
from app.scodoc import html_sidebar
import sco_version


# Some constants:

# Multiselect menus are used on a few pages and not loaded by default
BOOTSTRAP_MULTISELECT_JS = [
    "libjs/bootstrap/js/bootstrap.min.js",
    "libjs/bootstrap-multiselect-1.1.2/bootstrap-multiselect.min.js",
    "libjs/purl.js",
]

BOOTSTRAP_MULTISELECT_CSS = [
    "libjs/bootstrap/css/bootstrap.min.css",
    "libjs/bootstrap/css/bootstrap-theme.min.css",
    "libjs/bootstrap-multiselect-1.1.2/bootstrap-multiselect.min.css",
]


def standard_html_header():
    """Standard HTML header for pages outside depts"""
    # not used in ZScolar, see sco_header
    return f"""<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>
<title>ScoDoc: accueil</title>
<META http-equiv="Content-Type" content="text/html; charset={scu.SCO_ENCODING}">
<META http-equiv="Content-Style-Type" content="text/css">
<META name="LANG" content="fr">
<META name="DESCRIPTION" content="ScoDoc: gestion scolarite">

<link href="{scu.STATIC_DIR}/css/scodoc.css" rel="stylesheet" type="text/css"/>

</head><body>{scu.CUSTOM_HTML_HEADER_CNX}"""


def standard_html_footer():
    """Le pied de page HTML de la page d'accueil."""
    return f"""<p class="footer">
Problème de connexion (identifiant, mot de passe): <em>contacter votre responsable ou chef de département</em>.</p>
<p>Probl&egrave;mes et suggestions sur le logiciel: <a href="mailto:{scu.SCO_USERS_LIST}">{scu.SCO_USERS_LIST}</a></p>
<p><em>ScoDoc est un logiciel libre développé par Emmanuel Viennet.</em></p>
</body></html>"""


_HTML_BEGIN = f"""<!DOCTYPE html>
<html lang="fr">

<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=%(encoding)s" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta name="LANG" content="fr" />
<meta name="DESCRIPTION" content="ScoDoc" />
<title>%(page_title)s</title>

<link type="text/css" rel="stylesheet" href="{scu.STATIC_DIR}/libjs/jquery-ui-1.10.4.custom/css/smoothness/jquery-ui-1.10.4.custom.min.css" />

<link href="{scu.STATIC_DIR}/css/scodoc.css" rel="stylesheet" type="text/css" />
<link href="{scu.STATIC_DIR}/css/menu.css" rel="stylesheet" type="text/css" />
<script src="{scu.STATIC_DIR}/libjs/menu.js"></script>
<script src="{scu.STATIC_DIR}/libjs/bubble.js"></script>
<script>
 window.onload=function(){{enableTooltips("gtrcontent")}};
</script>

<script src="{scu.STATIC_DIR}/jQuery/jquery.js"></script>
<script src="{scu.STATIC_DIR}/jQuery/jquery-migrate-1.2.0.min.js"></script>
<script src="{scu.STATIC_DIR}/libjs/jquery.field.min.js"></script>

<script src="{scu.STATIC_DIR}/libjs/jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.min.js"></script>

<script src="{scu.STATIC_DIR}/libjs/qtip/jquery.qtip-3.0.3.min.js"></script>
<link type="text/css" rel="stylesheet" href="{scu.STATIC_DIR}/libjs/qtip/jquery.qtip-3.0.3.min.css" />

<script src="{scu.STATIC_DIR}/js/scodoc.js"></script>
<script src="{scu.STATIC_DIR}/js/etud_info.js"></script>
"""


def scodoc_top_html_header(page_title="ScoDoc: bienvenue"):
    H = [
        _HTML_BEGIN % {"page_title": page_title, "encoding": scu.SCO_ENCODING},
        """</head><body id="gtrcontent">""",
        scu.CUSTOM_HTML_HEADER_CNX,
    ]
    return "\n".join(H)


# Header:
def sco_header(
    # optional args
    page_title="",  # page title
    no_side_bar=False,  # hide sidebar
    cssstyles=(),  # additionals CSS sheets
    javascripts=(),  # additionals JS filenames to load
    scripts=(),  # script to put in page header
    bodyOnLoad="",  # JS
    init_qtip=False,  # include qTip
    init_google_maps=False,  # Google maps
    init_datatables=True,
    titrebandeau="",  # titre dans bandeau superieur
    head_message="",  # message action (petit cadre jaune en haut) DEPRECATED
    user_check=True,  # verifie passwords temporaires
    etudid=None,
    formsemestre_id=None,
):
    """Main HTML page header for ScoDoc
    Utilisé dans les anciennes pages. Les nouvelles pages utilisent le template Jinja.
    """
    from app.scodoc.sco_formsemestre_status import formsemestre_page_title

    if etudid is not None:
        g.current_etudid = etudid
    scodoc_flash_status_messages()

    # Get head message from http request:
    if not head_message:
        if request.method == "POST":
            head_message = request.form.get("head_message", "")
        elif request.method == "GET":
            head_message = request.args.get("head_message", "")
    params = {
        "page_title": page_title or sco_version.SCONAME,
        "no_side_bar": no_side_bar,
        "ScoURL": scu.ScoURL(),
        "encoding": scu.SCO_ENCODING,
        "titrebandeau_mkup": "<td>" + titrebandeau + "</td>",
        "authuser": current_user.user_name,
    }
    if bodyOnLoad:
        params["bodyOnLoad_mkup"] = """onload="%s" """ % bodyOnLoad
    else:
        params["bodyOnLoad_mkup"] = ""
    if no_side_bar:
        params["margin_left"] = "1em"
    else:
        params["margin_left"] = "140px"

    H = [
        """<!DOCTYPE html><html lang="fr">
<head>
<meta charset="utf-8"/>
<title>%(page_title)s</title>
<meta name="LANG" content="fr" />
<meta name="DESCRIPTION" content="ScoDoc" />

"""
        % params
    ]
    # jQuery UI
    # can modify loaded theme here
    H.append(
        f"""
        <link type="text/css" rel="stylesheet"
            href="{scu.STATIC_DIR}/libjs/jquery-ui-1.10.4.custom/css/smoothness/jquery-ui-1.10.4.custom.min.css" />
        <link type="text/css" rel="stylesheet"
            href="{scu.STATIC_DIR}/libjs/timepicker-1.3.5/jquery.timepicker.min.css" />
        """
    )
    if init_google_maps:
        # It may be necessary to add an API key:
        H.append('<script src="https://maps.google.com/maps/api/js"></script>')

    # Feuilles de style additionnelles:
    for cssstyle in cssstyles:
        H.append(
            f"""<link type="text/css" rel="stylesheet" href="{scu.STATIC_DIR}/{cssstyle}" />\n"""
        )

    H.append(
        f"""
<link href="{scu.STATIC_DIR}/css/scodoc.css" rel="stylesheet" type="text/css" />
<link href="{scu.STATIC_DIR}/css/menu.css" rel="stylesheet" type="text/css" />
<link href="{scu.STATIC_DIR}/css/gt_table.css" rel="stylesheet" type="text/css" />

<script src="{scu.STATIC_DIR}/libjs/menu.js"></script>
<script src="{scu.STATIC_DIR}/libjs/bubble.js"></script>
<script>
 window.onload=function(){{enableTooltips("gtrcontent")}};

 const SCO_URL="{scu.ScoURL()}";
 const SCO_TIMEZONE="{scu.TIME_ZONE}";
</script>"""
    )

    # jQuery
    H.append(
        f"""
    <script src="{scu.STATIC_DIR}/jQuery/jquery.js"></script>
    <script src="{scu.STATIC_DIR}/libjs/jquery.field.min.js"></script>
    """
    )
    # qTip
    if init_qtip:
        H.append(
            f"""<script src="{scu.STATIC_DIR}/libjs/qtip/jquery.qtip-3.0.3.min.js"></script>
        <link type="text/css" rel="stylesheet"
            href="{scu.STATIC_DIR}/libjs/qtip/jquery.qtip-3.0.3.min.css" />
        """
        )

    H.append(
        f"""<script
        src="{scu.STATIC_DIR}/libjs/jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.min.js"></script>
        <script src="{scu.STATIC_DIR}/libjs/timepicker-1.3.5/jquery.timepicker.min.js"></script>
        <script src="{scu.STATIC_DIR}/js/scodoc.js"></script>
        """
    )
    if init_google_maps:
        H.append(
            f'<script src="{scu.STATIC_DIR}/libjs/jquery.ui.map.full.min.js"></script>'
        )
    if init_datatables:
        H.append(
            f"""<link rel="stylesheet" type="text/css" href="{scu.STATIC_DIR}/DataTables/datatables.min.css"/>
        <script src="{scu.STATIC_DIR}/DataTables/datatables.min.js"></script>"""
        )
    # H.append(
    #    f'<link href="{scu.STATIC_DIR}/css/tooltip.css" rel="stylesheet" type="text/css" />'
    # )
    # JS additionels
    for js in javascripts:
        H.append(f"""<script src="{scu.STATIC_DIR}/{js}"></script>\n""")

    H.append(
        f"""<style>
#gtrcontent {{
   margin-left: {params["margin_left"]};
   height: 100%%;
   margin-bottom: 16px;
}}
</style>
"""
    )
    # Scripts de la page:
    if scripts:
        H.append("""<script>""")
        for script in scripts:
            H.append(script)
        H.append("""</script>""")

    H.append("</head>")

    # Body et bandeau haut:
    H.append("""<body %(bodyOnLoad_mkup)s>""" % params)
    H.append(scu.CUSTOM_HTML_HEADER)
    #
    if not no_side_bar:
        H.append(html_sidebar.sidebar(etudid))
    H.append("""<div id="gtrcontent">""")
    # En attendant le replacement complet de cette fonction,
    # inclusion ici des messages flask
    H.append(render_template("flashed_messages.j2"))
    #
    # Barre menu semestre:
    H.append(formsemestre_page_title(formsemestre_id))

    #
    if head_message:
        H.append('<div class="head_message">' + html.escape(head_message) + "</div>")
    #
    # div pour affichage messages temporaires
    H.append('<div id="sco_msg" class="head_message"></div>')
    #
    return "".join(H)


def sco_footer():
    """Main HTMl pages footer"""
    return (
        """</div><!-- /gtrcontent -->""" + scu.CUSTOM_HTML_FOOTER + """</body></html>"""
    )


def html_sem_header(
    title, with_page_header=True, with_h2=True, page_title=None, **args
):
    "Titre d'une page semestre avec lien vers tableau de bord"
    # sem now unused and thus optional...
    if with_page_header:
        h = sco_header(page_title="%s" % (page_title or title), **args)
    else:
        h = ""
    if with_h2:
        return h + f"""<h2 class="formsemestre">{title}</h2>"""
    else:
        return h
