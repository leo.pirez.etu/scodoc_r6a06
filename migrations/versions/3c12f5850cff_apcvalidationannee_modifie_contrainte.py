"""ApcValidationAnnee: modifie contrainte

Revision ID: 3c12f5850cff
Revises: f95656fdd3ef
Create Date: 2022-12-19 23:12:29.382528

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "3c12f5850cff"
down_revision = "f95656fdd3ef"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(
        "apc_validation_annee_etudid_annee_scolaire_key",
        "apc_validation_annee",
        type_="unique",
    )
    op.create_unique_constraint(
        None, "apc_validation_annee", ["etudid", "annee_scolaire", "ordre"]
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, "apc_validation_annee", type_="unique")
    op.create_unique_constraint(
        "apc_validation_annee_etudid_annee_scolaire_key",
        "apc_validation_annee",
        ["etudid", "annee_scolaire"],
    )
    # ### end Alembic commands ###
