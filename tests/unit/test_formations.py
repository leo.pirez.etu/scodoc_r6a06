# -*- mode: python -*-
# -*- coding: utf-8 -*-

""" Test creation/edition/import/export formations
"""

# test écrit par Fares Amer, mai 2021 et porté sur ScoDoc 8 en juillet 2021

# Créer 2 formations, une test et une normale. Créer 2 semestres dans la
# formation normale et un dans la formation test, créer 2 semestres dans la
# formation normale (un test et un normal), 2 ue (un test et un normal), 2
#  modules (un test et un normal) et 2 matieres (test et normal). Et dans la
#  formations test, un semestre, un module, un ue et une matiere. Afficher la
#  liste de tout ca puis supprimer les ue, mod, mat et sem test ainsi que la
#  formation test. Afficher la liste des UE, formations et modules restante.
#
#  Vérification :
#
#  - Les listes initiales comprennent bien tout les éléments créés avec les bons
#    noms etc
#  - La supression s'est bien effectué au niveau de scodoc web et en python
#  - Vérifier que les fonctions listes font bien la mise à jour après suppression
#
# Fonction de l'API utilisé :
#
# - create_formation
# - create_ue
# - create_matiere
# - create_module
# - create_formsemestre
# - create_moduleimpl
# - formation_export
# - formsemestre_list
# - moduleimpl_list
# - do_module_impl_with_module_list
# - do_formsemestre_delete
# - module_list
# - do_module_delete
# - matiere_list
# - do_matiere_delete
# - ue_list
# - do_ue_delete
# - do_formation_delete

import json
import os

import pytest

from app import db
from app.models import Formation, ModuleImpl
from app.scodoc import sco_edit_formation, sco_formsemestre
from app.scodoc import sco_edit_matiere
from app.scodoc import sco_edit_module
from app.scodoc import sco_edit_ue
from app.scodoc import sco_exceptions
from app.scodoc import sco_formations
from app.scodoc import sco_formsemestre_edit
from app.scodoc import sco_moduleimpl
from app.views import notes

from tests.conftest import RESOURCES_DIR
from tests.unit import sco_fake_gen


def test_formations(test_client):
    """Test création/édition/import/export formations"""
    G = sco_fake_gen.ScoFake(verbose=False)

    # --- Création de formations

    formation_id = G.create_formation(
        acronyme="F1", titre="Formation 1", titre_officiel="Titre officiel 1"
    )
    # --- Objet Formation
    formation = db.session.get(Formation, formation_id)
    assert isinstance(formation, Formation)
    assert formation.acronyme == "F1"
    assert formation.titre == "Formation 1"
    assert formation.titre_officiel == "Titre officiel 1"
    assert formation.dept_id
    f_dict = formation.to_dict()
    assert isinstance(f_dict, dict)
    fields = {
        "acronyme",
        "titre_officiel",
        "commentaire",
        "departement",
        "formation_id",
        "type_parcours",
        "code_specialite",
        "referentiel_competence_id",
        "id",
        "version",
        "formation_code",
        "dept_id",
        "titre",
    }
    assert set(f_dict.keys()) == fields
    f_dict = formation.to_dict(with_departement=False)
    assert set(f_dict.keys()) == fields - {"departement"}

    # Création des UEs, matières, modules
    ue_id = G.create_ue(formation_id=formation_id, acronyme="TST1", titre="ue test")
    matiere_id = G.create_matiere(ue_id=ue_id, titre="matière test")
    module_id = G.create_module(
        matiere_id=matiere_id,
        code="TSM1",
        coefficient=1.0,
        titre="module test",
    )

    ue_id2 = G.create_ue(formation_id=formation_id, acronyme="TST2", titre="ue test2")
    matiere_id2 = G.create_matiere(ue_id=ue_id2, titre="matière test2")
    module_id2 = G.create_module(
        matiere_id=matiere_id2,
        code="TSM2",
        coefficient=1.0,
        titre="module test",
    )

    uet_id = G.create_ue(formation_id=formation_id, acronyme="TSTt", titre="ue testt")
    matiere_id3 = G.create_matiere(ue_id=uet_id, titre="matière testt")
    module_id_t = G.create_module(
        matiere_id=matiere_id3,
        code="TSMt",
        coefficient=1.0,
        titre="module test",
    )

    formation_id2 = G.create_formation(acronyme="", titre="Formation test")
    assert db.session.get(Formation, formation_id2)
    ue3 = G.create_ue(formation_id=formation_id2, acronyme="TST3", titre="ue test3")
    matiere_id4 = G.create_matiere(ue_id=ue3, titre="matière test3")
    module_id3 = G.create_module(
        matiere_id=matiere_id4,
        code="TSM3",
        coefficient=1.0,
        titre="module test3",
    )

    # --- Création et implémentation des semestres

    formsemestre_id1 = G.create_formsemestre(
        formation_id=formation_id,
        semestre_id=1,
        date_debut="01/01/2021",
        date_fin="30/06/2021",
    )

    formsemestre_id2 = G.create_formsemestre(
        formation_id=formation_id,
        semestre_id=2,
        date_debut="01/09/2020",
        date_fin="31/12/2020",
    )

    moduleimpl_id = G.create_moduleimpl(
        module_id=module_id,
        formsemestre_id=formsemestre_id1,
    )

    _ = G.create_moduleimpl(
        module_id=module_id2,
        formsemestre_id=formsemestre_id1,
    )

    _ = G.create_moduleimpl(
        module_id=module_id_t,
        formsemestre_id=formsemestre_id2,
    )

    formsemestre_idt = G.create_formsemestre(
        formation_id=formation_id2,
        semestre_id=3,
        date_debut="01/01/2021",
        date_fin="30/06/2021",
    )

    mi3 = G.create_moduleimpl(
        module_id=module_id3,
        formsemestre_id=formsemestre_idt,
    )

    # --- Export de formation vers JSON
    exp = sco_formations.formation_export(
        formation_id=formation_id, fmt="json", export_ids=True
    ).get_data(as_text=True)
    assert isinstance(exp, str)
    load_exp = json.loads(exp)

    assert load_exp["acronyme"] == "F1"
    assert load_exp["titre_officiel"] == "Titre officiel 1"
    assert load_exp["titre"] == "Formation 1"
    assert load_exp["formation_code"] == formation.formation_code
    assert len(load_exp["ue"]) == 3
    assert load_exp["ue"][0]["acronyme"] == "TST1"
    assert load_exp["ue"][0]["titre"] == "ue test"
    assert load_exp["formation_id"] == formation_id

    # --- Liste des semestres

    li_sem1 = notes.formsemestre_list(
        formsemestre_id=formsemestre_id1, fmt="json"
    ).get_data(as_text=True)
    assert isinstance(li_sem1, str)
    load_li_sem1 = json.loads(li_sem1)  # uniquement le semestre 1 dans la liste

    assert len(load_li_sem1) == 1
    sem1 = sco_formsemestre.get_formsemestre(formsemestre_id1)
    assert load_li_sem1[0]["date_fin"] == sem1["date_fin"]
    assert load_li_sem1[0]["semestre_id"] == sem1["semestre_id"]
    assert load_li_sem1[0]["formation_id"] == sem1["formation_id"]

    li_semf = notes.formsemestre_list(
        formation_id=formation_id,
        fmt="json",
    ).get_data(as_text=True)
    assert isinstance(li_semf, str)
    load_li_semf = json.loads(li_semf)

    assert load_li_sem1[0] in load_li_semf
    assert len(load_li_semf) == 2
    sem2 = sco_formsemestre.get_formsemestre(formsemestre_id2)
    assert load_li_semf[1]["semestre_id"] == sem2["semestre_id"]

    li_sem = notes.formsemestre_list(fmt="json").get_data(as_text=True)
    load_li_sem = json.loads(li_sem)

    assert len(load_li_sem) == 3
    assert load_li_semf[0] and load_li_semf[1] in load_li_sem

    # --- Liste des modules

    lim_sem1 = sco_moduleimpl.moduleimpl_list(formsemestre_id=sem1["formsemestre_id"])

    assert len(lim_sem1) == 2
    assert module_id in (
        lim_sem1[0]["module_id"],
        lim_sem1[1]["module_id"],
    )
    assert module_id2 in (
        lim_sem1[0]["module_id"],
        lim_sem1[1]["module_id"],
    )

    lim_modid = sco_moduleimpl.moduleimpl_list(module_id=module_id)

    assert len(lim_modid) == 1

    lim_modimpl_id = sco_moduleimpl.moduleimpl_list(moduleimpl_id=moduleimpl_id)
    # print(lim_modimpl_id)

    # ---- Test de moduleimpl_withmodule_list

    assert lim_modid == lim_modimpl_id  # doit etre le meme resultat

    liimp_sem1 = sco_moduleimpl.moduleimpl_withmodule_list(
        formsemestre_id=sem1["formsemestre_id"]
    )

    assert len(liimp_sem1) == 2
    assert module_id in (
        liimp_sem1[0]["module_id"],
        liimp_sem1[1]["module_id"],
    )
    assert module_id2 in (
        liimp_sem1[0]["module_id"],
        liimp_sem1[1]["module_id"],
    )
    liimp_sem2 = sco_moduleimpl.moduleimpl_withmodule_list(
        formsemestre_id=sem2["formsemestre_id"]
    )
    assert module_id_t == liimp_sem2[0]["module_id"]
    liimp_modid = sco_moduleimpl.moduleimpl_withmodule_list(module_id=module_id)
    assert len(liimp_modid) == 1

    liimp_modimplid = sco_moduleimpl.moduleimpl_withmodule_list(
        moduleimpl_id=moduleimpl_id
    )

    assert liimp_modid == liimp_modimplid

    # --- Suppression du module, matiere et ue test du semestre 2

    # on doit d'abord supprimer le semestre:

    sco_formsemestre_edit.do_formsemestre_delete(
        formsemestre_id=sem2["formsemestre_id"]
    )

    li_module = sco_edit_module.module_list()
    assert len(li_module) == 4
    # Suppression impossible car utilisé dans le semestre formsemestre_idt:
    module3 = db.session.get(ModuleImpl, mi3).module
    with pytest.raises(sco_exceptions.ScoNonEmptyFormationObject):
        sco_edit_module.module_delete(module_id=module3.id)

    sco_formsemestre_edit.do_formsemestre_delete(formsemestre_idt)

    li_module2_before = sco_edit_module.module_list()

    sco_edit_module.do_module_delete(module3.id)
    sco_edit_module.do_module_delete(module_id_t)

    # deuxieme methode de supression d'un module
    li_module2_after = sco_edit_module.module_list()

    assert (
        len(li_module2_after) == len(li_module2_before) - 2
    )  # verification de la suppression

    lim_sem2 = sco_moduleimpl.moduleimpl_list(formsemestre_id=sem2["formsemestre_id"])

    assert len(lim_sem2) == 0  # deuxieme vérification si le module s'est bien sup

    li_mat = sco_edit_matiere.matiere_list()
    assert len(li_mat) == 4
    sco_edit_matiere.do_matiere_delete(oid=matiere_id3)  # on supprime la matiere
    li_mat2 = sco_edit_matiere.matiere_list()
    assert len(li_mat2) == 3  # verification de la suppression de la matiere

    li_ue = sco_edit_ue.ue_list()
    assert len(li_ue) == 4
    sco_edit_ue.ue_delete(ue_id=uet_id, dialog_confirmed=True)
    li_ue2 = sco_edit_ue.ue_list()
    assert len(li_ue2) == 3  # verification de la suppression de l'UE

    # --- Suppression d'une formation

    sco_edit_formation.do_formation_delete(formation_id=formation_id2)
    formation = db.session.get(Formation, formation_id2)
    assert formation is None


def test_import_formation(test_client, filename="formation-exemple-1.xml"):
    """Test import/export formations"""
    G = sco_fake_gen.ScoFake(verbose=False)

    # Lecture fichier XML local:
    with open(
        os.path.join(RESOURCES_DIR, "formations", filename),
        encoding="utf-8",
    ) as f:
        doc = f.read()

    # --- Création de la formation
    f = sco_formations.formation_import_xml(doc)
    assert len(f) == 3  # 3-uple
    formation_id = f[0]
    # --- Vérification des UE
    ues = sco_edit_ue.ue_list({"formation_id": formation_id})
    assert len(ues) == 10
    assert all(not ue["is_external"] for ue in ues)  # aucune UE externe dans le XML
    # --- Mise en place de 4 semestres
    formsemestre_ids = [
        G.create_formsemestre(
            formation_id=formation_id,
            semestre_id=x[0],
            date_debut=x[1],
            date_fin=x[2],
        )
        for x in (
            (1, "05/09/2019", "05/01/2020"),
            (2, "06/01/2020", "30/06/2020"),
            (3, "01/09/2020", "05/01/2021"),
            (4, "06/01/2021", "30/06/2021"),
        )
    ]
    # et les modules
    modules = sco_edit_module.module_list({"formation_id": formation_id})
    for mod in modules:
        moduleimpl_id = G.create_moduleimpl(
            module_id=mod["module_id"],
            formsemestre_id=formsemestre_ids[mod["semestre_id"] - 1],
        )
        mi = sco_moduleimpl.moduleimpl_list(moduleimpl_id=moduleimpl_id)[0]
        assert mi["ens"] == []
        assert mi["module_id"] == mod["module_id"]

    # --- Export formation en XML
    doc1 = sco_formations.formation_export(formation_id, fmt="xml").get_data(
        as_text=True
    )
    assert isinstance(doc1, str)
