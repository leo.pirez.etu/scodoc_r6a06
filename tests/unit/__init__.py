# ScoDoc Unit Tests

"""Set of unit tests for ScoDoc
"""


def call_view(view_function, *args, **kwargs):
    """Undecorate a view and call it directly."""
    # On a 3 décorateurs: @scodoc, @permission_required, @scodoc7func
    func = (
        view_function.__closure__[0]
        .cell_contents.__closure__[0]
        .cell_contents.__closure__[0]
        .cell_contents
    )
    assert func
    return func(*args, **kwargs)


def dict_include(d: dict, ref: dict) -> bool:
    """Checks that all keys in ref are in d, and with the same value."""
    # quick check
    ok = all((k in d) and (d[k] == ref[k]) for k in ref)
    if ok:
        return True
    # more details to ease debugging
    assert not (ref.keys() - d.keys()), "Keys missing"
    # identical keys, check values
    for k, v in ref.items():
        assert d[k] == v, "invalid value"
    return False
